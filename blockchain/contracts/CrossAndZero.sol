pragma solidity ^0.5.0;


contract CrossAndZero {
    enum SellState { Free, Cross, Zero }

    struct Game {
        uint id;
        SellState[9] cells;
        address firstPlayer;
        address secondPlayer;
        bool nextIsCross;
        address winner;
    }

    Game[] games;

    event gameStarted(uint gameId, address firstPlayer, address secondPlayer);
    event gameEnded(uint gameId, address firstPlayer, address secondPlayer, address winner);
    event cellTaken(uint gameId, uint8 cellIndex, bool isCross);

    modifier validGameId(uint gameId) {
        require(gameId > 0 && gameId <= games.length, "Unknown game ID");
        _;
    }

    function startGame(address opponent, bool startFirst) public returns (uint gameId) {
        require(opponent != address(0), "Opponent address not set");
        require(msg.sender != opponent, "Opponent must be another player");
        SellState[9] memory newGamecells;
        Game memory newGame = Game({
            id: games.length + 1,
            cells: newGamecells,
            firstPlayer: startFirst ? msg.sender : opponent,
            secondPlayer: startFirst ? opponent : msg.sender,
            nextIsCross: true,
            winner: address(0)
        });
        games.push(newGame);
        emit gameStarted(newGame.id, newGame.firstPlayer, newGame.secondPlayer);
        return newGame.id;
    }

    function takeCell(uint gameId, uint8 cellIndex) public validGameId(gameId) {
        Game storage game = games[gameId-1];
        require(game.winner == address(0), "Game ended");
        require(
            msg.sender == game.firstPlayer || msg.sender == game.secondPlayer,
            "You are not a player in the game"
        );
        address activePlayer = game.nextIsCross ? game.firstPlayer : game.secondPlayer;
        require(activePlayer == msg.sender, "Not your turn to take a cell");
        require(cellIndex < 9, "Cell index must be between 0 and 8");
        require(game.cells[cellIndex] == SellState.Free, "Cell already taken");

        game.cells[cellIndex] = game.nextIsCross
            ? SellState.Cross
            : SellState.Zero;
        game.nextIsCross = !game.nextIsCross;

        if (getGameEnded(game.cells, cellIndex)) {
            game.winner = msg.sender;
        }

        emit cellTaken(game.id, cellIndex, !game.nextIsCross);
        if (game.winner != address(0)) {
            emit gameEnded(game.id, game.firstPlayer, game.secondPlayer, game.winner);
        }
    }

    function getGameEnded(
        SellState[9] storage cells,
        uint8 lastCellIndex
    ) internal view returns(bool ended) {
        require(lastCellIndex < 9, "Cell index must be between 0 and 8");
        SellState lastSellState = cells[lastCellIndex];
        require(lastSellState != SellState.Free, "Last cell must be taken");

        // horizontal line
        uint8 firstHorizontalIndex = (lastCellIndex / uint8(3)) * uint8(3);
        if (
            cells[firstHorizontalIndex] == cells[firstHorizontalIndex+1] &&
            cells[firstHorizontalIndex] == cells[firstHorizontalIndex+2]
        ) {
            return true;
        }

        // vertical line
        uint firstVerticalIndex = lastCellIndex % uint8(3);
        if (
            cells[firstVerticalIndex] == cells[firstVerticalIndex+3] &&
            cells[firstVerticalIndex] == cells[firstVerticalIndex+6]
        ) {
            return true;
        }

        // top-left to bottom-right diagonal
        if (
            cells[0] == lastSellState &&
            cells[4] == lastSellState &&
            cells[8] == lastSellState
        ) {
            return true;
        }

        // top-right to bottom-left diagonal
        if (
            cells[2] == lastSellState &&
            cells[4] == lastSellState &&
            cells[6] == lastSellState
        ) {
            return true;
        }

        return false;
    }

    function getGame(uint gameId) public view validGameId(gameId) returns(
        SellState[9] memory cells,
        address firstPlayer,
        address secondPlayer,
        bool nextIsCross,
        address winner
    ) {
        Game storage game = games[gameId-1];
        return (
            game.cells,
            game.firstPlayer,
            game.secondPlayer,
            game.nextIsCross,
            game.winner
        );
    }

}