const truffleAssert = require('truffle-assertions');
const CrossAndZero = artifacts.require("CrossAndZero");

contract("CrossAndZero", (accounts) => {

    // --- unility functions ---

    function gameCellsToString(game) {
        const c = game.cells;
        return `${c[0]}${c[1]}${c[2]}-${c[3]}${c[4]}${c[5]}-${c[6]}${c[7]}${c[8]}`;
    }

    function assertGameState(game, expectedGameState) {
        assert.equal(gameCellsToString(game), expectedGameState.cells);
        assert.equal(game.firstPlayer, expectedGameState.firstPlayer);
        assert.equal(game.secondPlayer, expectedGameState.secondPlayer);
        assert.equal(game.nextIsCross, expectedGameState.nextIsCross);
        assert.equal(game.winner, expectedGameState.winner);
    }

    async function assertRejected (awaitable, rejectionReason) {
        try {
            await awaitable;
        } catch (e) {
            assert.equal(e.reason, rejectionReason);
        }
    }

    // --- tests ---

    it("Test game start", async () => {
        const crossAndZero = await CrossAndZero.deployed();

        // --- successfull game start ---

        const startGameTxOne = await crossAndZero.startGame(accounts[1], true);
        truffleAssert.eventEmitted(startGameTxOne, 'gameStarted', (ev) => {
            assert.equal(ev.gameId, 1);
            assert.equal(ev.firstPlayer, accounts[0]);  // first player
            assert.equal(ev.secondPlayer, accounts[1]);
            return true;
        });

        const startGameTxTwo = await crossAndZero.startGame(accounts[2], false);
        truffleAssert.eventEmitted(startGameTxTwo, 'gameStarted', (ev) => {
            assert.equal(ev.gameId, 2);
            assert.equal(ev.firstPlayer, accounts[2]);
            assert.equal(ev.secondPlayer, accounts[0]);  // second player
            return true;
        });

        // --- exceptions ---

        // against oneself
        await assertRejected(
            crossAndZero.startGame(accounts[0], true),
            "Opponent must be another player"
        )

        // against noone
        await assertRejected(
            crossAndZero.startGame("0x0000000000000000000000000000000000000000", true),
            "Opponent address not set"
        )
    });

    it("Test taking cell (making moves)", async() => {
        const crossAndZero = await CrossAndZero.deployed();

        // --- staring game --

        const gameStartTx = await crossAndZero.startGame(accounts[2], true, {from: accounts[1]});
        let gameId;
        truffleAssert.eventEmitted(gameStartTx, 'gameStarted', (ev) => {
            gameId = ev.gameId.toNumber();
            assert.equal(ev.firstPlayer, accounts[1]);
            assert.equal(ev.secondPlayer, accounts[2]);
            return true;
        });

        let expectedGameState = {
            cells: "000-000-000",
            firstPlayer: accounts[1],
            secondPlayer: accounts[2],
            nextIsCross: true,
            winner: "0x0000000000000000000000000000000000000000",
        }
        let game = await crossAndZero.getGame.call(gameId);
        assertGameState(game, expectedGameState);
        
        // --- all possible exceptions immediately after game start ---
        await assertRejected(
            crossAndZero.takeCell(gameId, 2, {from: accounts[0]}),
            "You are not a player in the game"
        );

        await assertRejected(
            crossAndZero.takeCell(gameId, 2, {from: accounts[2]}),
            "Not your turn to take a cell"
        );

        await assertRejected(
            crossAndZero.takeCell(gameId+1, 2, {from: accounts[2]}),
            "Unknown game ID"
        );

        await assertRejected(
            crossAndZero.takeCell(gameId, 10, {from: accounts[1]}),
            "Cell index must be between 0 and 8"
        );

        await assertRejected(
            crossAndZero.takeCell(gameId, -1, {from: accounts[1]}),
            "Cell index must be between 0 and 8"
        );
        
        // game state not changed
        game = await crossAndZero.getGame.call(gameId);
        assertGameState(game, expectedGameState);
        
        // --- successfull first move ---

        let tx = await crossAndZero.takeCell(gameId, 2, {from: accounts[1]});
        truffleAssert.eventEmitted(tx, 'cellTaken', (ev) => {
            assert.equal(ev.gameId, gameId);
            assert.equal(ev.cellIndex, 2);
            assert.equal(ev.isCross, true);
            return true;
        });

        game = await crossAndZero.getGame.call(gameId);

        expectedGameState.cells = "001-000-000";
        expectedGameState.nextIsCross = false;
        assertGameState(game, expectedGameState);

        // --- exceptions after first move ---

        await assertRejected(
            crossAndZero.takeCell(gameId, 4, {from: accounts[1]}),
            "Not your turn to take a cell"
        );

        await assertRejected(
            crossAndZero.takeCell(gameId, 2, {from: accounts[2]}),
            "Cell already taken"
        );

        // game state not changed
        game = await crossAndZero.getGame.call(gameId);
        assertGameState(game, expectedGameState);

        // --- successfull second move ---

        tx = await crossAndZero.takeCell(gameId, 4, {from: accounts[2]});
        truffleAssert.eventEmitted(tx, 'cellTaken', (ev) => {
            assert.equal(ev.gameId, gameId);
            assert.equal(ev.cellIndex, 4);
            assert.equal(ev.isCross, false);
            return true;
        });

        game = await crossAndZero.getGame.call(gameId);

        expectedGameState.cells = "001-020-000";
        expectedGameState.nextIsCross = true;
        assertGameState(game, expectedGameState);

        // --- successfull third move ---

        tx = await crossAndZero.takeCell(gameId, 5, {from: accounts[1]});
        truffleAssert.eventEmitted(tx, 'cellTaken', (ev) => {
            assert.equal(ev.gameId, gameId);
            assert.equal(ev.cellIndex, 5);
            assert.equal(ev.isCross, true);
            return true;
        });

        game = await crossAndZero.getGame.call(gameId);

        expectedGameState.cells = "001-021-000";
        expectedGameState.nextIsCross = false;
        assertGameState(game, expectedGameState);
    });

    it("Test game ended", async () => {
        const crossAndZero = await CrossAndZero.deployed();

        // -- winning move: 7 (horizantal) ---

        // staring game
        let gameStartTx = await crossAndZero.startGame(accounts[1], true, {from: accounts[0]});
        let gameId;
        truffleAssert.eventEmitted(gameStartTx, 'gameStarted', (ev) => {
            gameId = ev.gameId.toNumber();
            assert.equal(ev.firstPlayer, accounts[0]);
            assert.equal(ev.secondPlayer, accounts[1]);
            return true;
        });
        let expectedGameState = {
            cells: "000-000-000",
            firstPlayer: accounts[0],
            secondPlayer: accounts[1],
            nextIsCross: true,
            winner: "0x0000000000000000000000000000000000000000",
        }
        let game = await crossAndZero.getGame.call(gameId);
        assertGameState(game, expectedGameState);

        async function playMoves(moves) {
            for (let i=0; i < moves.length; i++) {
                await crossAndZero.takeCell(gameId, moves[i], {from: accounts[i % 2]});
            }
        }

        await playMoves([6, 4, 8, 5]);
        game = await crossAndZero.getGame.call(gameId);
        expectedGameState.cells = "000-022-101";
        assertGameState(game, expectedGameState);

        let winMoveTx = await crossAndZero.takeCell(gameId, 7, {from: accounts[0]});
        truffleAssert.eventEmitted(winMoveTx, 'gameEnded', (ev) => {
            assert.equal(ev.gameId, gameId);
            assert.equal(ev.firstPlayer, accounts[0]);
            assert.equal(ev.secondPlayer, accounts[1]);
            assert.equal(ev.winner, accounts[0]);
            return true;
        });
        game = await crossAndZero.getGame.call(gameId);
        expectedGameState.cells = "000-022-111";
        expectedGameState.nextIsCross = false;
        expectedGameState.winner = accounts[0];
        assertGameState(game, expectedGameState);

        // --- Exceptions ---
        await assertRejected(
            crossAndZero.takeCell(gameId, 3, {from: accounts[1]}),
            "Game ended"
        );
        // game not changed
        game = await crossAndZero.getGame.call(gameId);
        assertGameState(game, expectedGameState);

        // -- winning move: 5 (vertical) ---

        gameStartTx = await crossAndZero.startGame(accounts[1], true, {from: accounts[0]});
        truffleAssert.eventEmitted(gameStartTx, 'gameStarted', (ev) => {
            gameId = ev.gameId.toNumber();
            assert.equal(ev.firstPlayer, accounts[0]);
            assert.equal(ev.secondPlayer, accounts[1]);
            return true;
        });
        expectedGameState = {
            cells: "000-000-000",
            firstPlayer: accounts[0],
            secondPlayer: accounts[1],
            nextIsCross: true,
            winner: "0x0000000000000000000000000000000000000000",
        }
        game = await crossAndZero.getGame.call(gameId);
        assertGameState(game, expectedGameState);

        await playMoves([0, 2, 1, 8, 4]);
        game = await crossAndZero.getGame.call(gameId);
        expectedGameState.cells = "112-010-002";
        expectedGameState.nextIsCross = false;
        assertGameState(game, expectedGameState);

        winMoveTx = await crossAndZero.takeCell(gameId, 5, {from: accounts[1]});
        truffleAssert.eventEmitted(winMoveTx, 'gameEnded', (ev) => {
            assert.equal(ev.gameId, gameId);
            assert.equal(ev.firstPlayer, accounts[0]);
            assert.equal(ev.secondPlayer, accounts[1]);
            assert.equal(ev.winner, accounts[1]);
            return true;
        });
        game = await crossAndZero.getGame.call(gameId);
        expectedGameState.cells = "112-012-002";
        expectedGameState.nextIsCross = true;
        expectedGameState.winner = accounts[1];
        assertGameState(game, expectedGameState);

        // --- Exceptions ---
        await assertRejected(
            crossAndZero.takeCell(gameId, 3, {from: accounts[0]}),
            "Game ended"
        );
        // game not changed
        game = await crossAndZero.getGame.call(gameId);
        assertGameState(game, expectedGameState);

        // --- winning move: 4 (diagonal top-lert to bottom-right) ---

        gameStartTx = await crossAndZero.startGame(accounts[1], true, {from: accounts[0]});
        truffleAssert.eventEmitted(gameStartTx, 'gameStarted', (ev) => {
            gameId = ev.gameId.toNumber();
            assert.equal(ev.firstPlayer, accounts[0]);
            assert.equal(ev.secondPlayer, accounts[1]);
            return true;
        });
        expectedGameState = {
            cells: "000-000-000",
            firstPlayer: accounts[0],
            secondPlayer: accounts[1],
            nextIsCross: true,
            winner: "0x0000000000000000000000000000000000000000",
        }
        game = await crossAndZero.getGame.call(gameId);
        assertGameState(game, expectedGameState);

        await playMoves([0, 1, 8, 2]);
        game = await crossAndZero.getGame.call(gameId);
        expectedGameState.cells = "122-000-001";
        assertGameState(game, expectedGameState);

        winMoveTx = await crossAndZero.takeCell(gameId, 4, {from: accounts[0]});
        truffleAssert.eventEmitted(winMoveTx, 'gameEnded', (ev) => {
            assert.equal(ev.gameId, gameId);
            assert.equal(ev.firstPlayer, accounts[0]);
            assert.equal(ev.secondPlayer, accounts[1]);
            assert.equal(ev.winner, accounts[0]);
            return true;
        });
        game = await crossAndZero.getGame.call(gameId);
        expectedGameState.cells = "122-010-001";
        expectedGameState.nextIsCross = false;
        expectedGameState.winner = accounts[0];
        assertGameState(game, expectedGameState);

        // --- Exceptions ---
        await assertRejected(
            crossAndZero.takeCell(gameId, 3, {from: accounts[1]}),
            "Game ended"
        );
        // game not changed
        game = await crossAndZero.getGame.call(gameId);
        assertGameState(game, expectedGameState);
        
        // --- winning move: 2 (diagonal top-right to bottom-left) ---

        gameStartTx = await crossAndZero.startGame(accounts[1], true, {from: accounts[0]});
        truffleAssert.eventEmitted(gameStartTx, 'gameStarted', (ev) => {
            gameId = ev.gameId.toNumber();
            assert.equal(ev.firstPlayer, accounts[0]);
            assert.equal(ev.secondPlayer, accounts[1]);
            return true;
        });
        expectedGameState = {
            cells: "000-000-000",
            firstPlayer: accounts[0],
            secondPlayer: accounts[1],
            nextIsCross: true,
            winner: "0x0000000000000000000000000000000000000000",
        }
        game = await crossAndZero.getGame.call(gameId);
        assertGameState(game, expectedGameState);

        await playMoves([0, 2, 1, 4, 3]);
        game = await crossAndZero.getGame.call(gameId);
        expectedGameState.cells = "112-120-000",
        expectedGameState.nextIsCross = false;
        assertGameState(game, expectedGameState);

        winMoveTx = await crossAndZero.takeCell(gameId, 6, {from: accounts[1]});
        truffleAssert.eventEmitted(winMoveTx, 'gameEnded', (ev) => {
            assert.equal(ev.gameId, gameId);
            assert.equal(ev.firstPlayer, accounts[0]);
            assert.equal(ev.secondPlayer, accounts[1]);
            assert.equal(ev.winner, accounts[1]);
            return true;
        });
        game = await crossAndZero.getGame.call(gameId);
        expectedGameState.cells = "112-120-200",
        expectedGameState.nextIsCross = true;
        expectedGameState.winner = accounts[1];
        assertGameState(game, expectedGameState);

        // --- Exceptions ---
        await assertRejected(
            crossAndZero.takeCell(gameId, 5, {from: accounts[0]}),
            "Game ended"
        );
        // game not changed
        game = await crossAndZero.getGame.call(gameId);
        assertGameState(game, expectedGameState);
    });

});