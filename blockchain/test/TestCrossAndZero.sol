pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/CrossAndZero.sol";

contract TestCrossAndZero {

    function testStartGame() public {
        CrossAndZero crossAndZero = CrossAndZero(DeployedAddresses.CrossAndZero());
        uint firstGameId = crossAndZero.startGame(address(10), true);
        Assert.equal(firstGameId, 1, "Wrong game ID");
        uint secondGameId = crossAndZero.startGame(address(20), false);
        Assert.equal(secondGameId, 2, "Wrong game ID");
    }

}
