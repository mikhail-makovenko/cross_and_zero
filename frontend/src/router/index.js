import Vue from 'vue';
import VueRouter from 'vue-router';
import CrossAndZeroPlay from '@/views/cross_and_zero_play/CrossAndZeroPlay';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'index',
    redirect: {
      name: 'CrossAndZeroPlay',
    },
  },
  {
    path: '/game',
    name: 'CrossAndZeroPlay',
    component: CrossAndZeroPlay,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
